import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaisService {

  constructor() { }

  getPaises() {
    return [
        {
            'id': 'ABR', 'name': 'Brasil'
        }, {
            'id': 'ACL', 'name': 'Chile'
        }, {
                'id': 'ACO', 'name': 'Colômbia'
        }, {
                'id': 'ACR', 'name': 'Costa Rica'
        }, {
                'id': 'ACU', 'name': 'Cuba'
        }, {
                'id': 'ADO', 'name': 'República Dominicana'
        }, {
                'id': 'AEC', 'name': 'Equador'
        }, {
                'id': 'AFR', 'name': 'França'
        }, {
                'id': 'AGF', 'name': 'Guiana Francesa'
        }, {
                'id': 'AGT', 'name': 'Guatemala'
        }, {
                'id': 'AHN', 'name': 'Honduras'
        }, {
                'id': 'AHT', 'name': 'Haiti'
        }, {
                'id': 'AIT', 'name': 'Itália'
        }, {
                'id': 'AJM', 'name': 'Jamaica'
        }, {
                'id': 'AMX', 'name': 'México'
        }, {
                'id': 'ANI', 'name': 'Nicarágua'
        }, {
                'id': 'APA', 'name': 'Panamá'
        }, {
                'id': 'APE', 'name': 'Peru'
        }, {
                'id': 'APR', 'name': 'Porto Rico'
        }, {
                'id': 'APY', 'name': 'Paraguai'
        }, {
                'id': 'ASV', 'name': 'El Salvador'
        }, {
                'id': 'AUS', 'name': 'Estados Unidos'
        }, {
                'id': 'AUY', 'name': 'Uruguai'
        }, {
                'id': 'AVE', 'name': 'Venezuela'
        }
    ];
  }
}
