import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DashComponent } from './dash/dash.component';

export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'dash/:id', component: DashComponent }
];
