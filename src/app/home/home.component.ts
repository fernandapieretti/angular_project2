import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { Usuario } from '../shared/usuario.model';
import { CadastroService } from '../cadastro.service';
import { PaisService } from '../pais.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ CadastroService ]
})
export class HomeComponent implements OnInit {

  @ViewChild('formulario') public formulario: NgForm;
  public paises = [];

  constructor(
    private cadastroService: CadastroService,
    private paisService: PaisService,
    private router: Router
  ) { }

  ngOnInit() {
    this.paises = this.paisService.getPaises();
  }

  public camposInvalidos(): boolean {
    return false;
  }

  public confirmarCadastro(): void {

    if (this.formulario.valid) {

      let usuario: Usuario = new Usuario(
        this.formulario.value.email,
        this.formulario.value.password,
        this.formulario.value.fullName,
        this.formulario.value.birthDate,
        this.formulario.value.zipCode,
        this.formulario.value.streetName,
        this.formulario.value.number,
        this.formulario.value.complement,
        this.formulario.value.neighbourhood,
        this.formulario.value.country,
        this.formulario.value.state,
        this.formulario.value.city
      );

      this.cadastroService.cadastrarUsuario(usuario)
      .then( (response) =>  {
        this.router.navigate(['/dash/' + response ]);
      }).catch( (param: any) => {
        let message;
        switch (param.status) {
          case 400:
          message = JSON.parse(param._body).message;
          break;
          case 409:
          message = 'E-mail já cadastrado.';
          break;
          default:
          message = 'Ocorreu um erro inesperado, por favor, tente novamente.';
          break;
        }
        alert(message);
      });
    } else {
      alert('Preencha todos os campos.');
    }

  }

}
