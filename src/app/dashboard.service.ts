import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

const URLAcessos = 'https://www.improving.com.br/api/test/hits-by-browser';
const URLTemperatura = 'https://www.improving.com.br/api/test/city-temperatures';

@Injectable()
export class DashboardService {

    constructor(private http: Http) {}

    public getDadosAcessos(token): Promise<String> {
        const headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');
        return this.http.post(
            `${URLAcessos}`,
            JSON.stringify({'token': token}),
            new RequestOptions({ headers: headers }))
            .toPromise()
            .then((resposta) => resposta.json());
    }

    public getDadosTempo(token): Promise<String>  {
        const headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');
        return this.http.post(
            `${URLTemperatura}`,
            JSON.stringify({'token': token}),
            new RequestOptions({ headers: headers }))
            .toPromise()
            .then((resposta) => resposta.json());
    }

}
