import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { Usuario } from './shared/usuario.model';

const URL = 'https://www.improving.com.br/api/test/users';

@Injectable()
export class CadastroService {

    constructor(private http: Http) {}

    public cadastrarUsuario(usuario: Usuario): Promise<String> {
        const headers: Headers = new Headers();
        headers.append('Content-type', 'application/json');
        return this.http.post(
            `${URL}`,
            JSON.stringify(usuario),
            new RequestOptions({ headers: headers }))
        .toPromise()
        .then((resposta) => resposta.json().token);
    }
}
