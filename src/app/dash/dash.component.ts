import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { Chart } from 'node_modules/angular-highcharts';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css'],
  providers: [ DashboardService ]
})

export class DashComponent implements OnInit {

  constructor(
    private dashboardService: DashboardService,
    private route: ActivatedRoute
  ) { }

  public dadosAcessos = [];
  public dadosAcessosChart = [];
  public dadosMediaAcessos = 0;
  public totalAcessos = 0;
  public dadosTempo = [];
  public dadosTempoChart = [];
  public dadosMediaTemperatura = '';
  public chart = new Chart();
  public chartTempo = new Chart();
  public graphMode = true;
  public token = '';

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.token = this.route.snapshot.params.id;
    });

    this.dashboardService.getDadosAcessos(this.token)
      .then((resposta: any) => {
        this.dadosAcessos = resposta;
        this.totalAcessos = this.dadosAcessos.reduce((a, b) => a + b[1], 0 );
        this.bindChart();
      }).catch( (param: any) => {
        alert(this.errorHandling(param));
      });
    this.dashboardService.getDadosTempo(this.token)
      .then((resposta: any) => {
      this.dadosTempo = resposta;
      this.bindChartTempo();
    }).catch( (param: any) => {
      alert(this.errorHandling(param));
    });
  }

  public mediaTemperaturaMes(array, mes) {
    let filteredArray = array.filter((el) => el[0].includes('-' + mes, 0)).filter((el) => el[1] > 0);
    let sum = filteredArray.reduce((a, b) => a + b[1], 0 );
    return Number((sum / filteredArray.length).toFixed(1));
  }

  public mediaAcessos(acesso) {
    return Number((acesso * 100 / this.totalAcessos).toFixed(2));
  }

  public bindChart() {
    for (let i = 0, fLen = this.dadosAcessos.length; i < fLen; i++) {
      this.dadosMediaAcessos = this.mediaAcessos(this.dadosAcessos[i][1]);
      this.dadosAcessosChart.push( { 'name': this.dadosAcessos[i][0], 'data':  [ this.dadosMediaAcessos ]});
    }

    this.chart = new Chart({
        chart: {
          type: 'column'
        },
        title: {
          text: 'Browsers'
        },
        credits: {
          enabled: false
        },
        series: this.dadosAcessosChart
    });
  }

  public bindChartTempo() {
    let medias = [];
    for (let i = 0, fLen = this.dadosTempo.length; i < fLen; i++) {
      medias = [
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '01'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '02'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '03'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '04'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '05'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '06'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '07'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '08'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '09'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '10'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '11'),
        this.mediaTemperaturaMes(this.dadosTempo[i].data, '12')
      ];

      this.dadosTempoChart.push( { 'name': this.dadosTempo[i].name, 'data': medias });
    }

    this.chartTempo = new Chart({
        chart: {
          type: 'column'
        },
        title: {
          text: 'Temperature'
        },
        credits: {
          enabled: false
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Temperatura (ºC)'
            }
        },
        series: this.dadosTempoChart
    });
  }

  public toggleGraphMode() {
    this.graphMode = !this.graphMode;
  }

  public errorHandling(param) {
    let message;
    switch (param.status) {
      case 400:
      message = 'Há algum erro com o seu formato de requisição.';
      break;
      default:
      message = 'Ocorreu um erro inesperado, por favor, tente novamente.';
      break;
    }
    return message;
  }

}
