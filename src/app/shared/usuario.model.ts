export class Usuario {
    constructor(
        public email: string,
        public password: string,
        public fullName: string,
        public birthDate: string,
        public zipCode: string,
        public streetName: string,
        public number: number,
        public complement: string,
        public neighbourhood: string,
        public country: string,
        public state: string,
        public city: string
    ) {

    }
}
